# { Yazılım } lisansları
![](images/yl-image11.svg)

---

<section id="yazılım">

# { Yazılım } nedir?

Note: Yazılım, Elektronik (Bilgisayar) aygıtların belirli bir işi yapmasını sağlayan programların tümüne verilen isimdir. Bir başka deyişle, var olan bir problemi çözmek amacıyla bilgisayar dili kullanılarak oluşturulmuş anlamlı komutlar bütünüdür.

--v--

#### { Yazılım } sektörünün büyüklüğü

Note:İlgili linkinler; 
[En Değerli Şirketler](https://shiftdelete.net/dunya-en-degerli-sirketler-2023)


| Sıra    | Şirket Adı | Değeri (dolar)     |
| ------- | -------    | -------            |
|   1	  | Apple	   |    3.076 Trilyon   |
|   2     |	Microsoft  |	2.500 Trilyon   |
|   3	  | Saudi Aramco |	2.081 Trilyon   |
|   4     | Alphabet (Google) |1.672 Trilyon|
|   5	  | Amazon	   |    1.351 Trilyon   |



--v--

# alt slaytlar
<section id="alt">
alt **bilgileri** yazabilirsiniz
[x]sdjkjds
- sakdjsakj
  

--v--

| Kolon 1 | Kolon 2 | Kolon 3 |
| ------- | ------- | ------- |
| Satır   | Satır   | Satır   |
| Satır   | Satır   | Satır   |

---

##  son slayt!!!!!
